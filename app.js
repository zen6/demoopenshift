/**
 * Module dependencies.
 */

var express = require("express");
var http = require("http");
var path = require("path");
var favicon = require("serve-favicon");
var morgan = require("morgan");
var os = require("os");

var app = express();

app.set("port", process.env.PORT || 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "pug");

app.use(favicon(path.join(__dirname, "public", "ico", "favicon.ico")));
app.use(morgan("combined"));

app.use(function(req, res, next) {
  app.locals.pretty = true;
  next();
});

app.use(express.static(path.join(__dirname, "public")));

app.get("/", function(req, res) {
  res.render("default", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/fluid", function(req, res) {
  res.render("layouts/fluid" , {hostname: os.hostname, uptime: os.uptime });
});

app.get("/hero", function(req, res) {
  res.render("layouts/hero", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/marketing", function(req, res) {
  res.render("layouts/marketing-alternate", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/narrow", function(req, res) {
  res.render("layouts/marketing-narrow", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/signin", function(req, res) {
  res.render("layouts/signin", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/starter", function(req, res) {
  res.render("layouts/starter-template", {hostname: os.hostname, uptime: os.uptime });
});

app.get("/sticky", function(req, res) {
  res.render("layouts/sticky-footer", {hostname: os.hostname, uptime: os.uptime });
});

http.createServer(app).listen(app.get("port"), function() {
  console.log("Express server listening on port " + app.get("port"));
});
